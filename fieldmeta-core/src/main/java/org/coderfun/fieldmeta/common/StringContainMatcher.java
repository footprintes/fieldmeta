package org.coderfun.fieldmeta.common;

import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;

public class StringContainMatcher {
	public static ExampleMatcher build(String ...fields){
		ExampleMatcher matcher=ExampleMatcher.matching();
		for(String field:fields){
			matcher.withMatcher(field, GenericPropertyMatcher.of(StringMatcher.CONTAINING).ignoreCase());			
		}
		return matcher;
	}
}
