package org.coderfun.fieldmeta.common;

public interface EmailCaptchaService {
	
	final static String EMAIL_CAPTCHA_KEY_PREFIX = "e-captcha:";
	/**
	 * 发送验证码
	 * 
	 * @param email
	 */
	public void send(String email);

	public boolean valid(String email, String captcha);
}
