package org.coderfun.fieldmeta.common;

import org.coderfun.common.exception.IErrorCode;
/**
 * 
 * //2 04 001 释义：  04 = workspace  业务模块标识，001为具体的错误代码
 * @author klguang
 *
 */
public enum FieldmetaErrorCode implements IErrorCode{
	
	WORKSPACE_NOT_CREATED			(400,204001L,"工作空间尚未创建！");

	Long code;
	int httpStatus;
	String messageFormat;
	
	private FieldmetaErrorCode(int httpStatus, Long code, String messageFormat) {
		this.httpStatus = httpStatus;
		this.code = code;
		this.messageFormat = messageFormat;
	}
	
	@Override
	public int getHttpStatus() {
		// TODO Auto-generated method stub
		return this.httpStatus;
	}

	@Override
	public long getCode() {
		// TODO Auto-generated method stub
		return this.code;
	}

	@Override
	public String getMessageFormat() {
		// TODO Auto-generated method stub
		return this.messageFormat;
	}

}
