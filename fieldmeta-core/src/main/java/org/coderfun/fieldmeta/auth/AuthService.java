package org.coderfun.fieldmeta.auth;

import org.coderfun.member.core.MemberHolder;

public interface AuthService extends MemberHolder{

	final String MEMBER_ATTRIBUTE_NAME="member";
	final String WORKSPACE_KEY_PREFIX = "member-wp:";
	
	
	public void login(String email,String plainPassword);
	public void logout();
	
	public Long getWorkspaceId();
	public void changeWorkspace(Long workspaceId);
	
}
