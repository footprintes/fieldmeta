package org.coderfun.fieldmeta.dao;

import klg.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.PageField;

public interface PageFieldDAO extends BaseRepository<PageField, Long> {

}
