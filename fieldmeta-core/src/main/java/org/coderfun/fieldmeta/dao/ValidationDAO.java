package org.coderfun.fieldmeta.dao;

import klg.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.Validation;

public interface ValidationDAO extends BaseRepository<Validation, Long> {

}
