package org.coderfun.fieldmeta.dao;

import klg.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.Project;

public interface ProjectDAO extends BaseRepository<Project, Long> {

}
