package org.coderfun.fieldmeta.service;

import klg.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.EntityField;

public interface EntityFieldService extends BaseService<EntityField, Long>{

}
