package org.coderfun.fieldmeta.service;

import klg.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.Project;

public interface ProjectService extends BaseService<Project, Long>{

	public void changeDefault(Project project);
	
}
