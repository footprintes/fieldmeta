package org.coderfun.fieldmeta.service;

import java.math.BigDecimal;
import java.util.List;

import org.coderfun.fieldmeta.auth.AuthService;
import org.coderfun.fieldmeta.dao.EntityFieldDAO;
import org.coderfun.fieldmeta.dao.PageFieldDAO;
import org.coderfun.fieldmeta.dao.TablemetaDAO;
import org.coderfun.fieldmeta.entity.EntityField;
import org.coderfun.fieldmeta.entity.PageField;
import org.coderfun.fieldmeta.entity.Tablemeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import klg.common.dataaccess.BaseServiceImpl;

@Service
public class TablemetaServiceImpl  extends BaseServiceImpl<Tablemeta, Long> implements TablemetaService{
	@Autowired
	TablemetaDAO tablemetaDAO;
	
	@Autowired
	PageFieldDAO pageFieldDAO ;

	@Autowired
	EntityFieldDAO entityFieldDAO;
	
	@Autowired
	AuthService authService;
	
	
	@Override
	@Transactional
	public void saveFields(String tableName, List<EntityField> entityFields, List<PageField> pageFields) {
		// TODO Auto-generated method stub
		Long workspaceId = authService.getWorkspaceId();
		for(int i=0 ; i <entityFields.size();i++){
			EntityField entityField = entityFields.get(i);
			//设置workspaceId
			entityField.setWorkspaceId(workspaceId);
			
			entityField.setTableName(tableName);
			entityField.setColumnSort(BigDecimal.valueOf(i));
			entityFieldDAO.save(entityField);
			
			PageField pageField=pageFields.get(i);
			//设置workspaceId
			pageField.setWorkspaceId(workspaceId);
			
			pageField.setEntityField(entityField);
			pageField.setTableName(tableName);
			
			pageFieldDAO.save(pageField);
		}
	}	
}
