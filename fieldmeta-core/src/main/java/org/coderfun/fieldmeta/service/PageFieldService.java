package org.coderfun.fieldmeta.service;

import klg.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.PageField;

public interface PageFieldService extends BaseService<PageField, Long>{

}
