package org.coderfun.fieldmeta.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import klg.common.dataaccess.entity.BaseEntity_;

@Generated(value="Dali", date="2018-12-12T13:25:09.962+0800")
@StaticMetamodel(Validation.class)
public class Validation_ extends BaseEntity_ {
	public static volatile SingularAttribute<Validation, String> code;
	public static volatile SingularAttribute<Validation, String> description;
	public static volatile SingularAttribute<Validation, String> javaValid;
	public static volatile SingularAttribute<Validation, String> jsValid;
	public static volatile SingularAttribute<Validation, String> message;
	public static volatile SingularAttribute<Validation, String> name;
	public static volatile SingularAttribute<Validation, Long> workspaceId;
}
