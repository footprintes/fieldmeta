package org.coderfun.workspace.dict.service;

import org.coderfun.workspace.dict.entity.WPCodeItem;

import klg.common.dataaccess.BaseService;

public interface WPCodeItemService extends BaseService<WPCodeItem, Long>{

}
