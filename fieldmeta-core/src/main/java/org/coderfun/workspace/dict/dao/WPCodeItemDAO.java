package org.coderfun.workspace.dict.dao;

import org.coderfun.workspace.dict.entity.WPCodeItem;

import klg.common.dataaccess.BaseRepository;

public interface WPCodeItemDAO extends BaseRepository<WPCodeItem, Long> {

}
