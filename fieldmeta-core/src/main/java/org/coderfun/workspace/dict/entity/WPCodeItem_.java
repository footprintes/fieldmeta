package org.coderfun.workspace.dict.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import klg.common.dataaccess.entity.OrderEntity_;

@Generated(value="Dali", date="2018-12-12T14:30:32.118+0800")
@StaticMetamodel(WPCodeItem.class)
public class WPCodeItem_ extends OrderEntity_ {
	public static volatile SingularAttribute<WPCodeItem, String> classCode;
	public static volatile SingularAttribute<WPCodeItem, String> code;
	public static volatile SingularAttribute<WPCodeItem, String> name;
	public static volatile SingularAttribute<WPCodeItem, String> value;
	public static volatile SingularAttribute<WPCodeItem, String> extension;
	public static volatile SingularAttribute<WPCodeItem, String> pinyin;
	public static volatile SingularAttribute<WPCodeItem, Long> workspaceId;
}
