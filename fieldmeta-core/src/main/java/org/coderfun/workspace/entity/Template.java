package org.coderfun.workspace.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

import klg.common.dataaccess.entity.BaseEntity;

@Entity
@Table(name = "wp_template")
@Access(AccessType.FIELD)
public class Template extends BaseEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	/** 原创代码 */
	private Long originalCode;

	private String info;

	private Long memberId;
	private String memberEmail;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getOriginalCode() {
		return originalCode;
	}

	public void setOriginalCode(Long originalCode) {
		this.originalCode = originalCode;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getMemberEmail() {
		return memberEmail;
	}

	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}

}
