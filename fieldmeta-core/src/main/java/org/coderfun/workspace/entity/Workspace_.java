package org.coderfun.workspace.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import klg.common.dataaccess.entity.BaseEntity_;

@Generated(value="Dali", date="2018-12-12T13:24:14.502+0800")
@StaticMetamodel(Workspace.class)
public class Workspace_ extends BaseEntity_ {
	public static volatile SingularAttribute<Workspace, String> name;
	public static volatile SingularAttribute<Workspace, Long> memberId;
	public static volatile SingularAttribute<Workspace, String> memberEmail;
	public static volatile SingularAttribute<Workspace, Long> templateId;
}
