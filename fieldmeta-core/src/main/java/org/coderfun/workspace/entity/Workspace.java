package org.coderfun.workspace.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import klg.common.dataaccess.entity.BaseEntity;

@Entity
@Table(name = "wp_workspace")
public class Workspace extends BaseEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private Long memberId;
	private String memberEmail;
	
	private Long templateId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getMemberEmail() {
		return memberEmail;
	}

	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}
}
