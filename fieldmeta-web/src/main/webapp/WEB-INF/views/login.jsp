<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtm1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>fieldmeta登录</title>
<link href="${pageContext.request.contextPath}/static/css/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/static/easyui/jquery-easyui-1.5.3/jquery.min.js"></script>

<script>

var submtUrl = "${pageContext.request.contextPath}/member/login";

//防止iframe内出现登录
if(window.parent !== window){
	getTopWinow().location.href = "${pageContext.request.contextPath}/login";
}

/**
 * 在页面中任何嵌套层次的窗口中获取顶层窗口
 * @return 当前页面的顶层窗口对象
 */
function getTopWinow(){
    var p = window;
    while(p != p.parent){
        p = p.parent;
    }
    return p;
}

</script>

</head>

<body>
	<div class="login-main">
	<c:import url="/WEB-INF/views/_loginFromHeader.jsp"></c:import>
		<div class="login-content">
			<h2>用户登录</h2>
			<form method="post" id="login-form" name="login-form">
				<div class="login-info">
					<span class="user">&nbsp;</span>
					<input name="email" id="email" placeholder="邮箱" type="text" class="login-input" />
				</div>
				<div class="login-info">
					<span class="pwd">&nbsp;</span>
					<input name="password" id="password" placeholder="密码" type="password" class="login-input" />
				</div>
				<div class="login-error">
					<div id="errorPlacement"></div>
				</div>
				<div class="login-oper">
					<input id="submit" type="button" value="登 录" class="login-btn button" />
					<input id="resetButton" type="button" value="重 置" class="login-reset button" />
				</div>
				<div class="login-link">
					没有账号？<a href = "${pageContext.request.contextPath}/register">马上注册</a>
					<span>忘记密码？<a href="${pageContext.request.contextPath}/resetPassword">重置密码</a></span>
				</div>
			</form>
		</div>
		<c:import url="/WEB-INF/views/_loginFooter.jsp"/>
	</div>
	
	
<script type="text/javascript">

var errorPlacement = $("#errorPlacement");
function validateEmail(){
	//邮箱正则表达式
	var reg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$"); 
    var $email = $('#email');
    var email = $email.val().trim();
    if(!email){
    	errorPlacement.html("请输入邮箱！");
    	$email.focus();
    	return false;
    }
    if(!reg.test(email)){
    	errorPlacement.html("邮箱不合法！");
    	$email.focus();
    	return false;
    }
    return true;
}

	function validate(){
        var password = $('#password').val().trim();
        if(!validateEmail()){
        	return false;
        }
        if(!password){
        	errorPlacement.html("请输入密码！");
        	return false;
        }
        return true;
	}

	$(function(){
		// 为document绑定onkeydown事件监听是否按了回车键
		$(document).keydown(function(event) {
			if (event.keyCode === 13) { // 按了回车键
				$("#submit").trigger("click");
			}
		});
		
		$("#submit").click(function(){
			if(validate()){
				var data=$("#login-form").serializeArray();
				$.ajax({
					url : submtUrl,
					data : data,
					type : "POST",
					dataType : "json",
					success : function(json){
						if(json.type == "success"){
							window.location.href = "${pageContext.request.contextPath}/";
						}
					},
					error:function(XMLHttpRequest,textStatus,error){
				        var json = JSON.parse(XMLHttpRequest.responseText);
				        $("#errorPlacement").html(json.message);
				        refreshCaptcha();
					}
				});
			}
		})
		$("#resetButton").click(function(){
			$("#login-form")[0].reset();
			$("#errorPlacement").html("");
		});
		
	});
	

</script>
</body>
</html>
