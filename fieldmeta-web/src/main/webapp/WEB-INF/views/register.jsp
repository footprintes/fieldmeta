<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtm1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>fieldmeta注册</title>
<link href="${pageContext.request.contextPath}/static/css/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/static/easyui/jquery-easyui-1.5.3/jquery.min.js"></script>
</head>

<body>
	<div class="login-main">
	<c:import url="/WEB-INF/views/_loginFromHeader.jsp"></c:import>
		<div class="login-content">
			<h2>用户注册</h2>
			
			<form method="post" id="login-form" name="login-form">
				<div class="login-info">
					<span class="user">&nbsp;</span>
					<input name="username" id="username" type="text" class="login-input" />
				</div>
				<div class="login-info">
					<span class="user">&nbsp;</span>
					<input name="email" id="email" placeholder="邮箱" type="text" class="login-input"  />
				</div>
				<div class="login-info">
					<span class="pwd">&nbsp;</span>
					<input name="password" id="password" placeholder="密码" type="password" class="login-input" />
				</div>
				<div class="login-info">
					<span class="captcha">&nbsp;</span>
					<input name="captcha" id="captcha"  placeholder="邮箱验证码" style="width: 179px" type="text" class="login-input" />
					<input type="button" id="captcha-btn" value="获取验证码" class="captcha-btn button"/>
				</div>
				<div class="login-error">
					<div id="errorPlacement"></div>
				</div>
				<div class="login-oper">
					<input id="submit" type="button" value="注册" class="login-btn button" />
					<input id="resetButton" type="button" value="重 置" class="login-reset button" />
				</div>
				<div class="login-link">
				已有账号？<a href = "${pageContext.request.contextPath}/login">登录</a>
				</div>
				
			</form>
		</div>
		<c:import url="/WEB-INF/views/_loginFooter.jsp"/>
	</div>
	
	
<script type="text/javascript">
var submtUrl = "${pageContext.request.contextPath}/member/register";
var sendCaptchaUrl = "${pageContext.request.contextPath}/member/sendRegisterCaptcha";
var captchaExpire = ${captchaExpire};


$(function(){
	$("#captcha-btn").click(function(){
		if(validateEmail()){
			sendCaptcha();
		}
	});
	
	
	// 为document绑定onkeydown事件监听是否按了回车键
	$(document).keydown(function(event) {
		if (event.keyCode === 13) { // 按了回车键
			$("#submit").trigger("click");
		}
	});
	
	$("#submit").click(function(){
		if(validate()){
			var data=$("#login-form").serializeArray();
			$.ajax({
				url : submtUrl,
				data : data,
				type : "POST",
				dataType : "json",
				success : function(json){
					if(json.type == "success"){
						//重定向到登录页面
						window.location.href = "${pageContext.request.contextPath}/login";
					}
				},
				error:function(XMLHttpRequest,textStatus,error){
			        var json = JSON.parse(XMLHttpRequest.responseText);
			        $("#errorPlacement").html(json.message);
				}
			});
		}
	})
	$("#resetButton").click(function(){
		$("#login-form")[0].reset();
		$("#errorPlacement").html("");
	});
});

var errorPlacement = $("#errorPlacement");
	function validateEmail(){
		//邮箱正则表达式
		var reg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$"); 
        var $email = $('#email');
        var email = $email.val().trim();
        if(!email){
        	errorPlacement.html("请输入邮箱！");
        	$email.focus();
        	return false;
        }
        if(!reg.test(email)){
        	errorPlacement.html("邮箱不合法！");
        	$email.focus();
        	return false;
        }
        return true;
	}
	
	function validateUsername(){
		var $username = $('#username');
        var username = $username.val().trim();
        if(!username){
        	errorPlacement.html("请输入用户名！");
        	$username.focus();
        	return false;
        }
        if(username.length < 5){
        	errorPlacement.html("用户名长度>=5！");
        	$username.focus();
        	return false;
        }
        return true;
	}
	
	function validatePassword(){
		var $password = $('#password');
		var password = $password.val().trim();
        if(!password){
        	errorPlacement.html("请输入密码！");
        	$password.focus();
        	return false;
        }
        if(password.length < 5){
        	errorPlacement.html("密码长度>=5！");
        	$password.focus();
        	return false;
        }
        return true;
	}
	

	function validate(){
        var $captcha = $('#captcha');

		if(!validateUsername()){
			return false;
		}
        
        if(!validateEmail()){
        	return false;
        }
        
        if(!validatePassword()){
        	return false;
        }
         
        if(!$captcha.val().trim()){
        	errorPlacement.html("请输入验证码！");
        	$captcha.focus();
        	return false;
        }

        return true;
	}

	function sendCaptcha(){
		var code = $("#captcha-btn");
	    code.attr("disabled","disabled");
		$.ajax({
			url : sendCaptchaUrl,
			data : {email:$("#email").val()},
			type : "POST",
			dataType : "json",
			success : function(json){
				countdownCaptcha(code);
			},
			error:function(XMLHttpRequest,textStatus,error){
		        var json = JSON.parse(XMLHttpRequest.responseText);
		        $("#errorPlacement").html(json.message);
			    enableCaptcha();		        	
			}
		})
	}
	
	function enableCaptcha(){
		$("#captcha-btn").attr("disabled",false).val("获取验证码");
	}
	
	function countdownCaptcha(code){
	    var time = captchaExpire;
	    var set=setInterval(function(){
	    code.val(--time);
	    }, 1000);
	    setTimeout(function(){
		    code.attr("disabled",false).val("获取验证码");
		    clearInterval(set);
	    }, time*1000);
	}

</script>
</body>
</html>
