package org.coderfun.fieldmeta.web.workspace;



import java.util.List;

import org.coderfun.workspace.dict.entity.WPCodeItem;
import org.coderfun.workspace.dict.entity.WPCodeItem_;
import org.coderfun.workspace.dict.service.WPCodeItemService;
import org.coderfun.workspace.interceptor.WorkspaceValid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import klg.common.model.EasyUIPage;
import klg.common.model.JsonData;
import klg.common.model.JsonData.Type;
import klg.query.jpa.expr.AExpr;




@Controller("adminWPCodeItemController")
@RequestMapping("/admin/action/codeitem")
public class WPCodeItemController {
	@Autowired
	WPCodeItemService codeItemService;
	
	@ResponseBody
	@RequestMapping("/add")
	public JsonData add(
			@ModelAttribute WPCodeItem codeItem){
		
		if(codeItemService.getOne(AExpr.eq(WPCodeItem_.classCode, codeItem.getClassCode()),AExpr.eq(WPCodeItem_.code, codeItem.getCode()))==null)
			codeItemService.save(codeItem);
		else
			JsonData.success().setType(Type.error).setMessage("重复的代码！");
		return JsonData.success();
	}
	
	
	@ResponseBody
	@RequestMapping("/edit")
	public JsonData edit(
			@ModelAttribute WPCodeItem codeItem){
		
		codeItemService.update(codeItem);
		return JsonData.success();
	}
	
	@WorkspaceValid(entityClass = WPCodeItem.class)
	@ResponseBody
	@RequestMapping("/delete")
	public JsonData delete(
			@RequestParam Long id){
		
		codeItemService.delete(id);
		return JsonData.success();
	}
	
	@ResponseBody
	@RequestMapping("/findpage")
	public EasyUIPage findpage(
			@ModelAttribute WPCodeItem codeItem,
			@RequestParam int page,
			@RequestParam int rows){
		Pageable pageable=new PageRequest(page<1?0:page-1, rows, new Sort(Direction.DESC,"orderNum"));
		Page<WPCodeItem> pageData=codeItemService.findPage(codeItem, pageable);
		return new EasyUIPage(pageData);
	}
	
	@ResponseBody
	@RequestMapping("/findlist")
	public JsonData findlist(
			@ModelAttribute WPCodeItem codeItem){
		
		List<WPCodeItem> listData=codeItemService.findList(codeItem, new Sort(Direction.DESC,"orderNum"));
		return JsonData.success(listData);
	}
	
	@ResponseBody
	@RequestMapping("/datalist")
	public List datalist(
			@ModelAttribute WPCodeItem codeItem){
		List<WPCodeItem> listData=codeItemService.findList(codeItem, new Sort(Direction.DESC,"orderNum"));
		return listData;
	}
}
