package org.coderfun.fieldmeta.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2018-12-14T01:22:09.435+0800")
@StaticMetamodel(TypeMapping.class)
public class TypeMapping_ {
	public static volatile SingularAttribute<TypeMapping, String> sqlDialectCode;
	public static volatile SingularAttribute<TypeMapping, String> fullJavaType;
	public static volatile SingularAttribute<TypeMapping, String> javaType;
	public static volatile SingularAttribute<TypeMapping, String> sqlType;
	public static volatile SingularAttribute<TypeMapping, String> needJoinColumn;
	public static volatile SingularAttribute<TypeMapping, Long> workspaceId;
}
